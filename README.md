Pitot is a simple GPS/GLONASS speedometer for your SailfishOS device. It shows your current speed in big,
friendly letters and can convert to multiple different speed units.

Pitot is open source. Check the LICENCE file for details.

The development of this app has been graciously sponsored by [Vincit Oy](http://www.vincit.com/).