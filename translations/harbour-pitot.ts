<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pitot is open source software. You can find the licence details and source code on BitBucket.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open project website in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The development of this app has been graciously sponsored by Vincit Oy, the passionate software company.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Vincit website in browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Getting location…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Getting location…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Location disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please enable location services in settings.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Display unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>logic</name>
    <message>
        <source>meters per second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>meters per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilometers per hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>miles per hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>feet per second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>knots</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
